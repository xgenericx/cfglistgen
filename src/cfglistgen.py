import xlwt3 as xlwt
from xlwt3 import easyxf
import var
import sys
import os.path
from datetime import datetime

style1 = xlwt.easyxf(num_format_str='D-MMM-YY')

inputpath = sys.argv[1]
input = open(inputpath,'r')

tl = easyxf('border: left medium, top medium')
t = easyxf('border: top medium')
tr = easyxf('border: right medium, top medium')
r = easyxf('border: right medium, top thin, bottom thin;''align: wrap yes, horiz left, vert top;')
br = easyxf('border: right medium, bottom medium')
b = easyxf('border: bottom medium')
bl = easyxf('border: left medium, bottom medium')
l = easyxf('border: left medium, top thin, bottom thin;''align: wrap yes, horiz left, vert top;')
a = easyxf('border: left thin, right thin, top thin, bottom thin;''align: wrap yes, horiz left, vert top;')

wb = xlwt.Workbook()
ws = wb.add_sheet('CfgId List',cell_overwrite_ok=True)
ws.col(0).width = 500
ws.col(1).width = 4500
ws.col(2).width = 1200
ws.col(3).width = 3000
ws.col(4).width = 2000
ws.col(5).width = 5000
ws.col(6).width = 1500
ws.col(7).width = 15000
row = 0
cfgidold = ''
multi = 0
stringminrange = ''
stringmaxrange = ''
valueold=''
things = ['','']
for line in input:
    linecopy = line
    known = 0
    cfgid = line.split(';')[0].strip()
    port = line.split(';')[1].strip()
    value = line.split(';')[2].strip()
    mini = line.split(';')[3].strip()
    maxi = line.split(';')[4].strip()
    valuetype = line.split(';')[5].strip()
    cfgidtype = line.split(';')[6].strip()
    cfgid2= cfgid 
    port2= port 
    value2= value
    mini2= mini
    maxi2= maxi
    valuetype2=valuetype
    cfgidtype2=cfgidtype
    if cfgid == cfgidold:
        multi = 1
        lineold = line.strip()
        if value.isalpha():
            valueold = value.strip()
        if mini != '':
            stringminrange = stringminrange+', '+mini
        stringmaxrange = stringmaxrange+', '+maxi
    if cfgid != cfgidold:
        if multi == 1:
            row-=1
            cfgid = lineold.split(';')[0].strip()
            port = lineold.split(';')[1].strip()
            value = lineold.split(';')[2].strip()
            valuetype = lineold.split(';')[5].strip()
            cfgidtype = lineold.split(';')[6]
            if valueold == '':
                valueold = value
            if not known:
                if os.path.isfile(os.path.dirname(os.path.realpath(sys.argv[0]))+'\\var.txt'):
                    definitions = open (os.path.dirname(os.path.realpath(sys.argv[0]))+'\\var.txt','r')
                    for linex in definitions:
                        if linex.count("'")<4:
                            continue
                        things[0] = linex.split("'")[1]
                        things[1] = linex.split("'")[3]
                        if cfgid.lower() == things[0].lower():
                            known = 1
                            ws.write(row,1,cfgid,style=l)
                            ws.write(row,2,port,style=a)
                            ws.row(row).set_cell_text(3,valueold,style=a)
                            ws.write(row,4,stringminrange,style=a)
                            ws.write(row,5,stringmaxrange,style=a)
                            ws.write(row,6,valuetype,style=a)
                            ws.write(row,7,things[1],style=r)
                    definitions.close()
                else:
                    for things in var.contents:
                        if cfgid.lower() == things[0].lower():
                            known = 1
                            ws.write(row,1,cfgid,style=l)
                            ws.write(row,2,port,style=a)
                            ws.row(row).set_cell_text(3,valueold,style=a)
                            ws.write(row,4,stringminrange,style=a)
                            ws.write(row,5,stringmaxrange,style=a)
                            ws.write(row,6,valuetype,style=a)
                            ws.write(row,7,things[1],style=r)
            if not known:
                ws.write(row,1,cfgid,style=l)
                ws.write(row,2,port,style=a)
                ws.row(row).set_cell_text(3,valueold,style=a)
                ws.write(row,4,stringminrange,style=a)
                ws.write(row,5,stringmaxrange,style=a)
                ws.write(row,6,valuetype,style=a)
                ws.write(row,7,'',style=r)
            multi = 0
            stringminrange = ''
            stringmaxrange = ''
            row+=1
        if multi == 0:
            known = 0
            valueold = value
            if not known:
                if os.path.isfile(os.path.dirname(os.path.realpath(sys.argv[0]))+'\\var.txt'):
                    definitions = open (os.path.dirname(os.path.realpath(sys.argv[0]))+'\\var.txt','r')
                    for liney in definitions:
                        if liney.count("'")<4:
                            continue
                        things[0] = liney.split("'")[1]
                        things[1] = liney.split("'")[3]
                        if cfgid2.lower() == things[0].lower():
                            known = 1
                            ws.write(row,1,cfgid2,style=l)
                            ws.write(row,2,port2,style=a)
                            ws.row(row).set_cell_text(3,value2,style=a)
                            ws.write(row,4,mini2,style=a)
                            ws.write(row,5,maxi2,style=a)
                            ws.write(row,6,valuetype2,style=a)
                            ws.write(row,7,things[1],style=r)
                            row+=1
                    definitions.close()
                else:
                    for things in var.contents:
                        if cfgid2.lower() == things[0].lower():
                            known = 1
                            ws.write(row,1,cfgid2,style=l)
                            ws.write(row,2,port2,style=a)
                            ws.row(row).set_cell_text(3,value2,style=a)
                            ws.write(row,4,mini2,style=a)
                            ws.write(row,5,maxi2,style=a)
                            ws.write(row,6,valuetype2,style=a)
                            ws.write(row,7,things[1],style=r)
                            row+=1
            if not known:
                ws.write(row,1,cfgid2,style=l)
                ws.write(row,2,port2,style=a)
                ws.row(row).set_cell_text(3,value2,style=a)
                ws.write(row,4,mini2,style=a)
                ws.write(row,5,maxi2,style=a)
                ws.write(row,6,valuetype2,style=a)
                ws.write(row,7,'',style=r)
                row+=1
            stringminrange = mini.strip()
            stringmaxrange = maxi.strip()
    cfgidold = cfgid.strip()
        

wb.save(inputpath.rpartition('\\')[2].rpartition('.')[0]+'.xls')

